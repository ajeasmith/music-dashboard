import { createContext, useState } from 'react';
import HeaderComponent from './components/Header/HeaderComponent';
import SearchComponent from './components/Search/SearchComponent';
import WrapperComponent from './components/Wrapper/WrapperComponent';

export const AppContext = createContext();

export const AppContextProvider = (props) => {
	const [results, setResults] = useState([]);
	const [noresults, setNoresults] = useState('');
	const [isloading, setisloading] = useState(false);
	const fetchSongs = async (name) => {
		setisloading(true);
		try {
			const request = await fetch(
				`https://shazam.p.rapidapi.com/search?term=${name}&locale=en-US&offset=0&limit=5`,
				{
					method: 'GET',
					headers: {
						'x-rapidapi-key':
							'066599f04bmsh19f8179a797d45cp16c1cdjsna555aa6aa2f4',
						'x-rapidapi-host': 'shazam.p.rapidapi.com',
					},
				}
			);
			const response = await request.json();
			setResults(response.tracks.hits);
			console.log(response);
			setisloading(false);
			if (results === null) {
				setisloading(false);
				setNoresults(`No song or artist found ${name}`);
			} else {
				setNoresults('');
			}
		} catch (error) {
			setisloading(false);
			console.error(error);
		}
	};

	return (
		<AppContext.Provider
			value={{ fetchSongs, results, noresults, isloading }}
		>
			{props.children}
		</AppContext.Provider>
	);
};
function App() {
	return (
		<AppContextProvider>
			<HeaderComponent />
			<SearchComponent />
			<WrapperComponent />
		</AppContextProvider>
	);
}

export default App;
