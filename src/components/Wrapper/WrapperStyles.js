import styled from 'styled-components';

export const Wrapper = styled.section`
	max-width: 1230px;
	margin: 0 auto;
	display: flex;
	align-items: flex-start;
	column-gap: 24px;
	margin-bottom: 3em;
	@media (min-width: 1400px) {
		max-width: 1250px;
		align-items: flex-end;
	}
	@media (max-width: 1180px) {
		display: block;
	}
`;
export const Wrapper2 = styled.section`
	max-width: 1230px;
	margin: 0 auto;
	display: flex;
	padding: 0 1em;
	justify-content: space-between;

	@media (max-width: 1230px) {
		flex-direction: column;
	}
	@media (max-width: 580px) {
		padding: 0 2em;
	}
`;
