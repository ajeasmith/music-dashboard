import { Fragment } from 'react';
import ArtistComponent from '../Artist/ArtistComponent';
import SongResultsComponent from '../Song/SongResultsComponent';
import TrendingComponent from '../Trending/TrendingComponent';
import GenreComponent from '../Genre/GenreComponent';
import ArtistIconComponent from '../ArtistIcons/ArtistIconComponent';
import { Wrapper, Wrapper2 } from './WrapperStyles';
const WrapperComponent = () => {
	return (
		<Fragment>
			<Wrapper>
				<TrendingComponent />
				<ArtistComponent />
			</Wrapper>
			<Wrapper2>
				<SongResultsComponent />
				<div>
					<GenreComponent />
					<ArtistIconComponent />
				</div>
			</Wrapper2>
		</Fragment>
	);
};

export default WrapperComponent;
