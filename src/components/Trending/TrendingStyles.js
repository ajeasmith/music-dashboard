import styled from 'styled-components';
export const Section = styled.section`
	max-width: 870px;
	margin-top: 2.5em;
	padding-left: 1.2em;
	@media (min-width: 1400px) {
		max-width: 768px;
	}
	@media (max-width: 1180px) {
		max-width: none;
		width: 100%;
		padding: 0 2em;
	}
	@media (max-width: 580px) {
		display: none;
	}
`;
export const SlideContainer = styled.div`
	display: flex;
	flex-wrap: nowrap;
	overflow-x: hidden;
	position: relative;
`;
export const Heading = styled.h2`
	color: #3f4b5e;
	margin-bottom: 0.3em;
	font-family: Heebo, sans-serif;
`;
export const Slide = styled.div`
	flex-shrink: 0;
	background-size: cover;
	width: 100%;
	height: 467px;
	background-position: center;
	border-radius: 10px;
	position: relative;
`;
export const SlideContent = styled.div`
	color: white;
	width: 40%;
	max-width: 60%;
	margin: 5.7em 3em;
`;
export const Title = styled.h2`
	margin: 0.1em 0;
	font-size: 30px;
	font-family: Source Sans Pro, sans-serif;
	font-weight: bold;
`;
export const Location = styled.p`
	margin-bottom: 2.5em;
	font-size: 16px;
	color: white;
	font-family: Source Sans Pro, sans-serif;
	font-weight: 600;
`;
export const Play = styled.button`
	border: none;
	background: white;
	border-radius: 3px;
	width: 62px;
	height: 22px;
	color: #0094b6;
	margin-right: 5px;
`;
export const Share = styled.button`
	background: white;
	border: none;
	color: #0094b6;
	height: 22px;
	width: 24px;
	border-radius: 3px;
`;

export const IndicatorWrapper = styled.div`
	display: flex;
	justify-content: center;
	flex-wrap: nowrap;
	position: absolute;
	bottom: 0;
	right: 0;
	width: 100%;
	padding: 1em 0;
`;

export const Dot = styled.div`
	cursor: pointer;
	margin-right: 8px;
	width: 13px;
	height: 13px;
	border-radius: 12px;
	background-color: white;
`;
