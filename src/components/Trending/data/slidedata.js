export const slides = [
	{
		image: './slideimages/concert1.jpg',
		title: 'Red Snapper: Performance Review',
		location: 'Kamelia',
	},
	{
		image: './slideimages/concert2.jpg',
		title: 'Twenty one pilots: Performance Review',
		location: 'Detroit, Michigan',
	},
	{
		image: './slideimages/concert3.jpg',
		title: 'John Mayer: Performance Review',
		location: 'Fairfield County, Connecticut',
	},
	{
		image: './slideimages/concert4.jpg',
		title: 'The Black Keys: Performance Review',
		location: 'Franklin, Tennessee',
	},
	{
		image: './slideimages/concert5.jpg',
		title: 'Queen: Performance Review',
		location: 'Manchester, Great Britain',
	},
];
