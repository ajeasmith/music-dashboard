import { useEffect, useState } from 'react';
import play from '../../images/Play.png';
import share from '../../images/Share.png';
import { slides } from './data/slidedata';
import {
	Slide,
	Section,
	SlideContent,
	Title,
	Location,
	Play,
	Share,
	Dot,
	IndicatorWrapper,
	SlideContainer,
	Heading,
} from './TrendingStyles';

const TrendingComponent = () => {
	const [autoplaytime] = useState(4000);
	const [currentSlide, setcurrentSlide] = useState(0);

	const Indicator = ({ nextSlide }) => {
		return (
			<IndicatorWrapper>
				{Array(slides.length)
					.fill(1)
					.map((_, i) => {
						return (
							<Dot
								key={i}
								style={{
									backgroundColor:
										currentSlide === i ? '#0094B6' : 'white',
								}}
								onClick={() => nextSlide(i)}
							/>
						);
					})}
			</IndicatorWrapper>
		);
	};

	// eslint-disable-next-line
	const nextSlide = (slideIndex = currentSlide + 1) => {
		const newSlideIndex = slideIndex >= slides.length ? 0 : slideIndex;
		setcurrentSlide(newSlideIndex);
	};

	useEffect(() => {
		const timer = setTimeout(() => {
			nextSlide();
		}, autoplaytime);

		return () => clearTimeout(timer);
		// eslint-disable-next-line
	}, [currentSlide, autoplaytime]);

	return (
		<Section>
			<Heading>Trendings</Heading>
			<SlideContainer>
				{slides.map((slide, index) => {
					return (
						<Slide
							key={index}
							style={{
								backgroundImage: `url('${
									process.env.PUBLIC_URL + slide.image
								}')`,
								marginLeft:
									index === 0 ? `-${currentSlide * 100}%` : undefined,
							}}
						>
							<SlideContent>
								<p style={{ fontSize: '10px', fontWeight: '600' }}>
									{`// TRENDING`}
								</p>
								<Title>{slide.title}</Title>
								<Location>- {slide.location}</Location>

								<div className="play_btn">
									<Play>
										<span>PLAY</span>{' '}
										<img src={play} alt="play icon" />{' '}
									</Play>
									<Share>
										<img src={share} alt="share icon" />
									</Share>
								</div>
							</SlideContent>
							<Indicator nextSlide={nextSlide} />
						</Slide>
					);
				})}
			</SlideContainer>
		</Section>
	);
};

export default TrendingComponent;
