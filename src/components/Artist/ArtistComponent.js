import {
	Section,
	Heading,
	ArtistWrapper,
	ArtistImage,
	ArtistName,
	ArtistSongs,
	Album,
	ArtistTitle,
	ArtistName2,
	ArtistPara,
	ArtistSongList,
} from './ArtistStyles';
import { SongData } from './Song';
import SongItemComponent from '../Song/SongItemComponent';
const ArtistComponent = () => {
	return (
		<Section>
			<Heading>Artist of the week</Heading>
			<ArtistWrapper>
				<ArtistImage
					style={{
						backgroundImage: `url('${process.env.PUBLIC_URL}/images/monicalee.jpg')`,
					}}
				>
					<ArtistName>Monica Lee</ArtistName>
				</ArtistImage>
				<ArtistSongs>
					<div>
						<Album>{`// Album`}</Album>
						<ArtistTitle>Always Authentic</ArtistTitle>
						<ArtistName2>Monica Lee</ArtistName2>
						<ArtistPara>
							The artists we represent are one of the most successful in
							Virginia and also were a huge breakthrough in the
							international market, topping radio and sales around the
							world.
						</ArtistPara>
					</div>
					<div>
						<ArtistSongList>
							{SongData.map((song) => {
								return (
									<div key={song.number}>
										<SongItemComponent song={song} />
									</div>
								);
							})}
						</ArtistSongList>
					</div>
					<p
						style={{
							textAlign: 'center',
							color: '#5D6C76',
							fontFamily: 'Heebo',
						}}
					>
						Listen to full album
					</p>
				</ArtistSongs>
			</ArtistWrapper>
		</Section>
	);
};

export default ArtistComponent;
