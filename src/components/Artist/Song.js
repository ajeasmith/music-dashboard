import favorite from './images/Favourite.png';
import share from './images/Share.png';
import download from './images/Download.png';
import group from './images/Group.png';
import voice from './images/Voice.png';
export const SongData = [
	{
		number: 1,
		iconFav: favorite,
		songName: 'No more time',
		count: '42,822',
		time: '3:21',
		downloadIcon: download,
		shareIcon: share,
		groupIcon: group,
		voiceIcon: voice
	},
	{
		number: 2,
		iconFav: favorite,
		songName: 'Go Away',
		count: '67,420',
		time: '3:30',
		downloadIcon: download,
		shareIcon: share,
		groupIcon: group,
		voiceIcon: voice
	},
	{
		number: 3,
		iconFav: favorite,
		songName: 'With you',
		count: '38,556',
		time: '3:21',
		downloadIcon: download,
		shareIcon: share,
		groupIcon: group,
		voiceIcon: voice
	},
	{
		number: 4,
		iconFav: favorite,
		songName: 'Always authentic',
		count: '35,820',
		time: '3:20',
		downloadIcon: download,
		shareIcon: share,
		groupIcon: group,
		voiceIcon: voice
	},
	{
		number: 5,
		iconFav: favorite,
		songName: 'No more show',
		count: '51,432',
		time: '4:01',
		downloadIcon: download,
		shareIcon: share,
		groupIcon: group,
		voiceIcon: voice
	},
];
