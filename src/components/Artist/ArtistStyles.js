import styled from 'styled-components';

export const Section = styled.section`
	height: 467px;
	margin-top: 3em;
	flex: 1;
	@media (max-width: 1400px) {
		padding-right: 1.2em;
	}
	@media (max-width: 1180px) {
		display: none;
	}
`;
export const Heading = styled.h2`
	color: #3f4b5e;
	margin-bottom: 0.2em;

	@media (min-width: 1400px) {
		display: none;
	}
	@media (max-width: 1180px) {
		display: none;
	}
`;
export const ArtistImage = styled.div`
	background-size: cover;
	background-position: center;
	border-radius: 7px;
	height: 100%;
	width: 100%;

	@media (min-width: 1400px) {
		display: none;
	}
	@media (max-width: 1180px) {
		display: none;
	}
`;
export const ArtistName = styled.h1`
	color: white;
	font-family: 'Lancelot', cursive;
	font-size: 45px;
	font-weight: 400;
	text-align: center;
	padding: 0.7em 0;
`;
export const ArtistWrapper = styled.div`
	display: flex;
	height: 100%;
`;
export const ArtistSongs = styled.div`
	display: none;
	@media (min-width: 1400px) {
		display: flex;
		flex-direction: column;
	}
`;
export const Album = styled.small`
	font-weight: 600;
	font-family: Source Sans Pro, sans-serif;
	color: #2e3b52;
`;
export const ArtistTitle = styled.h1`
	font-size: 50px;
	font-weight: bold;
	font-family: Source Sans Pro, sans-serif;
	color: #2e3b52;
`;
export const ArtistName2 = styled.p`
	color: #a6acbe;
	font-size: 18px;
	font-weight: 600;
	font-family: Source Sans Pro, sans-serif;
`;
export const ArtistPara = styled.p`
	color: #a6acbe;
	margin-top: 0.5em;
	font-family: Source Sans Pro, sans-serif;
`;
export const ArtistSongList = styled.ul`
	list-style: none;
	padding: 0;
	margin: 2em 0;
	color: #0f1e36;
	opacity: 0.6;
	font-family: Heebo, sans-serif;
	font-weight: 500;
`;
