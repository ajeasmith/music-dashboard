import { useState, useContext } from 'react';
import { AppContext } from '../../App';
import { Section, Form, FormInput, Error } from './SearchStyles';
const SearchComponent = () => {
	const { fetchSongs } = useContext(AppContext);
	const [value, setValue] = useState('');
	const [error, setError] = useState('');
	const [isError, setisError] = useState(false);

	const handleChange = (e) => {
		setValue(e.target.value);
	};
	const handleSubmit = async (e) => {
		e.preventDefault();
		if (value === '') {
			setisError(true);
			setError('*Please enter a song or artist');
		} else {
			setisError(false);
			setError('');
			// submit data to API
			fetchSongs(value);
		}
	};
	const hideErrorMessage = () => {
		setisError(false);
		setError('');
	};

	return (
		<Section>
			<Form onSubmit={handleSubmit} className="form">
				<i className="fas fa-search" style={{ color: '#C4C4C4' }}></i>
				<FormInput
					value={value}
					onChange={(e) => handleChange(e)}
					type="text"
					placeholder="Search for new music, news, artists..."
					onBlur={hideErrorMessage}
				/>
			</Form>

			{isError ? <Error>{error}</Error> : null}
		</Section>
	);
};

export default SearchComponent;
