import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from '../../App';

describe('Search Component', () => {
	test('renders search component', () => {
		render(<App />);
		const search = screen.getByRole('textbox');
		expect(search).toBeInTheDocument();
	});
    test('input should contain text', () => {
        render(<App />);
        const searchInput = screen.getByRole('textbox');
        userEvent.type(searchInput, 'drake');
        expect(searchInput.value).toEqual('drake');
    });
});
