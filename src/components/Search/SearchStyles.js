import styled from 'styled-components';

export const Section = styled.section`
	display: flex;
	flex-direction: column;
	align-items: center;
	padding: 0 2em;
`;
export const Form = styled.form`
	display: flex;
	align-items: center;
	justify-content: center;
	margin-top: 3em;
	margin-bottom: 1em;
	backround: white;
	width: 100%;
	max-width: 700px;
	height: 60px;
	box-shadow: 0px 0px 20px 0px #c4c4c4;
	border-radius: 50px;
	background: white;
`;
export const FormInput = styled.input`
	width: 85%;
	outline: none;
	margin-left: 0.7em;
	border: none;
	font-size: 16px;
`;
export const Error = styled.p`
	color: red;
	font-style: italic;
`;
