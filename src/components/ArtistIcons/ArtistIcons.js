import artisticon1 from './Images/artisticon1.jpg';
import artisticon2 from './Images/artisticon2.jpg';
import artisticon3 from './Images/artisticon3.jpg';
import artisticon4 from './Images/artisticon4.jpg';
import artisticon5 from './Images/artisticon5.jpg';
import artisticon6 from './Images/artisticon6.jpg';

export const artistIconData = [
	{
		id: 1,
		image: artisticon1,
		name: 'Chaos theory',
	},
	{
		id: 2,
		image: artisticon2,
		name: 'Cutting corners',
	},
	{
		id: 3,
		image: artisticon3,
		name: 'No comment',
	},
	{
		id: 4,
		image: artisticon4,
		name: 'Leon Bridges',
	},
	{
		id: 5,
		image: artisticon5,
		name: 'District zero',
	},
	{
		id: 6,
		image: artisticon6,
		name: 'The last straw',
	},
];
