import styled from 'styled-components';

export const ImageContainer = styled.div`
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;
	margin: 2em 0;

	@media (max-width: 1230px) {
		justify-content: end;
	}
`;
export const Image = styled.img`
	border-radius: 50px;
	width: 88px;
	height: 88px;
	object-fit: cover;
`;
export const Name = styled.p`
	color: #0f1e36;
	font-size: 12px;
	font-family: Heebo, sans-serif;
	font-weight: 500;
`;
