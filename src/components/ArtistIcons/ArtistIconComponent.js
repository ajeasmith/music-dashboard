import React from 'react';
import { artistIconData } from './ArtistIcons';
import { ImageContainer, Image, Name } from './ArtistIconStyles';
const ArtistIconComponent = () => {
	return (
		<ImageContainer>
			{artistIconData.map((icon) => {
				return (
					<div
						style={{
							display: 'block',
							marginRight: '1em',
							marginBottom: '1em',
						}}
					>
						<Image src={icon.image} alt={icon.image} key={icon.id} />
						<Name style={{ textAlign: 'center', marginTop: '5px' }}>
							{icon.name}
						</Name>
					</div>
				);
			})}
		</ImageContainer>
	);
};

export default ArtistIconComponent;
