import audio from '../../images/audio.png';
import user from '../../images/ajea.jpg';
import {
	Header,
	Navbar,
	LeftDiv,
	RightDiv,
	UserImg,
	Name,
	Occupation,
	ListenCount,
} from './HeaderStyles';
const HeaderComponent = () => {
	return (
		<Header>
			<Navbar>
				<LeftDiv>
					<img src={audio} alt="audio" style={{ marginRight: '10px' }} />
					<ListenCount>8 people listening you</ListenCount>
				</LeftDiv>
				<RightDiv>
					<div style={{ marginRight: '10px' }}>
						<Name>Ajea Smith</Name>
						<Occupation>Artist</Occupation>
					</div>
					<div>
						<UserImg src={user} alt="ajea" />
					</div>
				</RightDiv>
			</Navbar>
		</Header>
	);
};

export default HeaderComponent;
