import styled from 'styled-components';

export const Header = styled.header`
	background: white;
	height: 100px;
	padding: 0 1.2em;
	font-family: Heebo, sans-serif;
	@media (max-width: 1180px) {
		padding: 0 2em;
	}
`;
export const Navbar = styled.nav`
	height: 100%;
	max-width: 1200px;
	margin: 0 auto;
	display: flex;
	align-items: center;
	justify-content: space-between;
`;
export const LeftDiv = styled.section`
	display: flex;
	align-items: center;
`;
export const RightDiv = styled.section`
	display: flex;
	align-items: center;
`;
export const UserImg = styled.img`
	width: 55px;
	border-radius: 100px;
`;
export const Name = styled.p`
	color: #2e3b52;
	font-weight: 500;
`;
export const Occupation = styled.p`
	text-align: right;
	color: #a6acbe;
	font-weight: 500;
	font-size: 12px;
`;
export const ListenCount = styled.p`
	font-size: 18px;
	font-weight: 500;
	color: #373b53;
`;
