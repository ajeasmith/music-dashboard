import styled from 'styled-components';

export const SongSection = styled.section`
	max-width: 1230px;
	margin: 0 auto;
	flex: 1;
	@media (max-width: 1230px) {
		margin-left: 0;
		margin-right: 0;
		margin-bottom: 1.8em;
	}
`;
export const Header = styled.h1`
	margin-bottom: 10px;
	font-size: 20px;
	font-weight: bold;
	font-family: Source Sans Pro, sans-serif;
	color: #2e3b52;
`;
export const Wrapper = styled.section`
	max-width: 1230px;
	display: flex;
`;
export const SongItem = styled.li`
	display: flex;
	padding: 0.7em;
	justify-content: space-between;

	:hover {
		background: white;
		cursor: pointer;
		border-radius: 3px;
		box-shadow: 0px 0px 9px 0px #a6acbe;
	}
`;

export const SongDetails = styled.div`
	display: flex;
	margin-left: 2em;
	justify-content: space-between;

	@media (max-width: 580px) {
		display: none;
	}
`;
export const SongName = styled.span`
	margin-left: 1em;
`;

export const Song = styled.p`
	margin-right: 3em;
	@media (min-width: 1400px) {
		margin-right: 1.5em;
	}
`;
export const SongTitle = styled.div`
	flex: 1;
	margin-left: 2.5em;
	@media (min-width: 1400px) {
		margin-left: 1em;
	}
`;

export const Results = styled.div`
	flex: 1;
	margin-right: 1em;
`;
