import React from 'react';
import { render, screen } from '@testing-library/react';
import SongResultsComponent from './SongResultsComponent';
import { AppContext } from '../../App';
import '@testing-library/jest-dom/extend-expect';

describe('Song Result Component', () => {
	test("it should render 'No songz to show' ", () => {
		const initial = {
			results: [],
			loading: false,
		};
		render(
			<AppContext.Provider value={{ ...initial }}>
				<SongResultsComponent />
			</AppContext.Provider>
		);
		expect(screen.getByText('No songz to show')).toBeInTheDocument();
		screen.debug();
	});

	test('it should render our data', () => {
		const initial = {
			results: [
				{
					track: {
						share: {
							subject: 'Wants and Needs - Drake Feat. Lil Baby',
						},
					},
				},
			],
			loading: false,
		};
		render(
			<AppContext.Provider value={{ ...initial }}>
				<SongResultsComponent />
			</AppContext.Provider>
		);
		expect(screen.getByTestId('displayed')).toBeInTheDocument();
		screen.debug();
	});
});
