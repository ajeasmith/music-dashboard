import { useState } from 'react';
import {
	SongDetails,
	SongName,
	Song,
	SongTitle,
	SongItem,
} from './SongResultStyles';

const SongItemComponent = ({ song }) => {
	const [hover, setHover] = useState(false);

	return (
		<SongItem
			onMouseOver={() => setHover(true)}
			onMouseOut={() => setHover(false)}
			key={song.number}
		>
			<div>
				{hover ? (
					<p>
						<img src={song.voiceIcon} alt="voice_recorder" />
					</p>
				) : (
					<p>{song.number}</p>
				)}
			</div>
			<SongTitle>
				<img src={song.iconFav} alt="favourite" />
				<SongName>{song.songName}</SongName>
			</SongTitle>
			<SongDetails>
				<Song>{song.count}</Song>
				<Song>{song.time}</Song>
				<Song>
					<img src={song.downloadIcon} alt="share" />
				</Song>
				<Song>
					<img src={song.shareIcon} alt="share" />
				</Song>
				<Song>
					<img src={song.groupIcon} alt="share" />
				</Song>
			</SongDetails>
		</SongItem>
	);
};

export default SongItemComponent;
