import React, { useContext } from 'react';
import { Wrapper, SongSection, Header, Results } from './SongResultStyles';
import { AppContext } from '../../App';
import SongResult from './SongResult';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import Loader from 'react-loader-spinner';

const SongResultsComponent = () => {
	const { results, isloading } = useContext(AppContext);

	return (
		<SongSection>
			<Header>Songz</Header>
			<Wrapper>
				<Results>
					{isloading && (
						<div data-testid="loading">
							<Loader
								type="ThreeDots"
								color="#00BFFF"
								height={80}
								width={80}
								timeout={5000} //3 secs
							/>
						</div>
					)}
					{!results.length ? (
						<p data-testid="song text">No songz to show</p>
					) : (
						<div data-testid="displayed">
							{results.map((track, index) => {
								return (
									<SongResult
										tracks={track}
										index={index}
										key={index}
									/>
								);
							})}
						</div>
					)}
				</Results>
			</Wrapper>
		</SongSection>
	);
};

export default SongResultsComponent;
