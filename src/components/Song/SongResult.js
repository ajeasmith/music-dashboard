import React, { useState } from 'react';
import {
	SongItem,
	SongDetails,
	SongName,
	Song,
	SongTitle,
} from './SongResultStyles';
import Download from '../Artist/images/Download.png';
import Share from '../Artist/images/Share.png';
import Favourite from '../Artist/images/Favourite.png';
import Group from '../Artist/images/Group.png';
import Voice from '../Artist/images/Voice.png';
const SongResult = ({ tracks, index }) => {
	const [hover, setHover] = useState(false);
	return (
		<SongItem
			onMouseOver={() => setHover(true)}
			onMouseOut={() => setHover(false)}
			key={index}
		>
			<div>
				{hover ? (
					<p>
						<img src={Voice} alt="voice_recorder" />
					</p>
				) : (
					<p>{index + 1}</p>
				)}
			</div>
			<SongTitle>
				<img src={Favourite} alt="favourite" />
				<SongName>{tracks.track.share.subject}</SongName>
			</SongTitle>
			<SongDetails>
				<Song>
					<img src={Download} alt="download" />
				</Song>
				<Song>
					<img src={Share} alt="share" />
				</Song>
				<Song>
					<img src={Group} alt="group" />
				</Song>
			</SongDetails>
		</SongItem>
	);
};

export default SongResult;
