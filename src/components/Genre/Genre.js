export const GenreData = [
	{
		id: 1,
		name: 'Blues',
	},
	{
		id: 2,
		name: 'Classical',
	},
	{
		id: 3,
		name: 'Country',
	},
	{
		id: 4,
		name: 'Dance',
	},
	{
		id: 5,
		name: 'Electronic',
	},
	{
		id: 6,
		name: 'Hip Hop',
	},
	{
		id: 7,
		name: 'Jazz',
	},
	{
		id: 8,
		name: 'Latin',
	},
	{
		id: 9,
		name: 'Metal',
	},
	{
		id: 10,
		name: 'Party',
	},
	{
		id: 11,
		name: 'R&B / Soul',
	},
	{
		id: 12,
		name: 'Reggae / Dancehall',
	},
	{
		id: 13,
		name: 'Soundtracks',
	},
	{
		id: 14,
		name: 'World',
	},
];
