import React from 'react';
import { Section, GenreUl, GenreLi, Heading } from './GenreStyles';
import { GenreData } from './Genre';

const GenreComponent = () => {
	return (
		<Section>
			<Heading>Popular genres</Heading>
			<GenreUl>
				{GenreData.map((genre) => {
					return <GenreLi key={genre.id}>{genre.name}</GenreLi>;
				})}
			</GenreUl>
		</Section>
	);
};

export default GenreComponent;
