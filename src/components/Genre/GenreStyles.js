import styled from 'styled-components';

export const Section = styled.section`
	flex: 1;
	max-width: 650px;
`;
export const Heading = styled.h1`
	margin-bottom: 10px;
	font-size: 20px;
	font-weight: bold;
	font-family: Source Sans Pro, sans-serif;
	color: #2e3b52;
`;
export const GenreUl = styled.ul`
	padding: 0;
	list-style: none;
	display: flex;
	flex-wrap: wrap;
	align-items: flex-start;
`;
export const GenreLi = styled.li`
	color: #373b53;
	font-size: 11px;
	text-align: center;
	font-family: Heebo, sans-serif;
	border: 1px solid #373b53;
	border-radius: 5px;
	margin-left: 10px;
	margin-bottom: 10px;
	padding: 8px 12px;

	&:hover {
		background-color: #1f94b6;
		border: 1px solid #1f94b6;
		color: white;
		cursor: pointer;
	}
`;
