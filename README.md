## Screenshots

![Music Dashboard laptop screens](https://i.ibb.co/6FrGYM9/large-musicdashboard.png)
![Music Dashboard mobile screens](https://i.ibb.co/5RDHGY9/mobile-musicdashboard.png)

## Features

-  Users can search for songs or artists and see their track details

## Technologies used in this project

-  React
-  Styled Components
-  React Testing Library
-  Font awesome
-  Trello
-  BitBucket
-  Figma for design

## How I worked on this project

-  I built this app inspired by this Figma design: ![Figma design](https://i.ibb.co/Q8QJfPr/figma-screenshot.png)
-  I worked with tasks on a Trello board: ![Trello tasks](https://i.ibb.co/BZzd8rc/trello2-screenshot.png)
-  I used feature branches and Pull Requests: [[Readme update PR](https://bitbucket.org/ajeasmith/music-dashboard/pull-requests/26/updated-readme-file)]
-  I used Responsive CSS with styled-components: [[Artist Component - styled component commit](https://bitbucket.org/ajeasmith/music-dashboard/commits/ee70e93ef509b1b19e37202fb604da47369b9ad2)]
-  I created integration tests using React Testing Library: [[Song Results Component - test commit](https://bitbucket.org/ajeasmith/music-dashboard/commits/e45cda0426ddc9d25d87956a6f8e9dac9cc34387)]

## Why I built the project this way

-  I decided to use Context API instead of a large state management like Redux, since my state is not large.
-  I used styled-components because it's a great library for styling. It includes an auto-prefixer, uses scoped classes, and allows a seamless integration with JS.
-  My plan is to become a front-end developer, which is the reason why I wanted to practice creating projects in a more professional way.
-  Testing is an essential part of production applications. I used React Testing Library and I covered the essential features of the app with tests.

## Available Scripts

In the project directory, you can run:

### [](https://github.com/AjeaSmith/ReactMedia#npm-start)`npm start`

Runs the app in the development mode.  
Open [http://localhost:3000](http://localhost:3000/) to view it in the browser.

The page will reload if you make edits.  
You will also see any lint errors in the console.

### [](https://github.com/AjeaSmith/ReactMedia#npm-test)`npm test`

Launches the test runner in the interactive watch mode.  
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### [](https://github.com/AjeaSmith/ReactMedia#npm-run-build)`npm run build`

Builds the app for production to the `build` folder.  
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.  
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### [](https://github.com/AjeaSmith/ReactMedia#npm-run-eject)`npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
